# IPRiboTagHippocampus
investigate neuronal cell type specific ribosome associated mRNAs in CA1 compartment of mouse hippocampus

### Experimental layout
Experimental [layout](./layout/experimentLayout_220817.pdf) is designed according the RiboTag methodology, where the following samples are collected:

* Somata layer (9 animals pool)
  * input x 2 replica
  * IP x 2 replica

* Neuropil layer (9 animals pool)
  * input x 2 replica
  * IP x 2 replica

### Sequencing
* Illumina NextSeq
* 400M reads flow cell
* 93% =< Q30 quality bases

```
bcl2fastq
--runfolder-dir /storage/scic/illuminanextseq/raw_data/170817_NB501737_0007_AHHMLHBGX3/
--output-dir /storage/scic/illuminanextseq/raw_data/170817_NB501737_0007_AHHMLHBGX3/fastq/
--no-lane-splitting
--processing-threads 12
```

### Alignment

#### prepare STAR-aligner genome index
Reads are aligned against [mm10](http://hgdownload.soe.ucsc.edu/goldenPath/mm10/bigZips/) version of mouse genome. The samples contain [ERCC](https://www.thermofisher.com/order/catalog/product/4456740) RNA spike-in mix with the following content:
* somata input replica 1 & 2 : 0.5ul of 1:10 dilution
* neuropil input replica 1 & 2 : 1.0ul of 1:100 dilution
* somata IP replica 1 & 2 : 0.5ul of 1:10 dilution
* neuropil IP replica 1 & 2 : 1.0ul of 1:100 dilution

```
STAR
--runMode genomeGenerate
--runThreadN 8
--genomeDir /storage/scic/Data/External/Bioinformatics/Mouse/Genomes/mm10_Dec2011/stardbercc/
--genomeFastaFiles /storage/scic/Data/External/Bioinformatics/Mouse/Genomes/mm10_Dec2011/mm10.fa /storage/scic/Data/External/Bioinformatics/Mouse/Genomes/mm10_Dec2011/ERCCSequences.fa
```

#### genome alignment
Reads are aligned using [STAR](https://github.com/alexdobin/STAR) and a custom written batch processing [script](./scripts/AlignSingleEndGz.sh/). Resulting BAM files are sorted by coordinates and indexed for random access using [Samtools](https://github.com/samtools/).

```
bash AlignSingleEndGz.sh
/storage/scic/illuminanextseq/raw_data/170817_NB501737_0007_AHHMLHBGX3/fastq/
/storage/scic/Data/External/Bioinformatics/Mouse/Genomes/mm10_Dec2011/stardbercc/
/storage/schu/ProjectGroups/Ribosome/IPRiboTag/bams/
/storage/schu/ProjectGroups/Ribosome/IPRiboTag/logs/
```

#### prepare annotation files
Annotation [GTF](http://mblab.wustl.edu/GTF2.html) files for mouse genome are downloaded using UCSC [Table Browser](http://genome.ucsc.edu/cgi-bin/hgTables) tool. UCSC export has the flow of keeping messenger RNA id in both fields. Example given `gene_id "NM_177407"; transcript_id "NM_177407";`, where one requires gene_id to contain official gene symbol, e.g. `gene_id "Camk2a"; transcript_id "NM_177407";`. To fix this issue a conversion table is downloaded from Table Browser tools using the following fields:
* mm10.refFlat.geneName :: name of gene as it appears in genome browser.
* mm10.refFlat.name :: mRNA reference sequence id.

To merge those use a custom Perl [script](./scripts/reMapGeneID.pl).

```
perl reMapGeneID.pl 
/storage/scic/Data/External/Bioinformatics/Mouse/Annotation/2017Aug/refSeqGenes_mm10_ReMap_23Aug2017.txt
/storage/scic/Data/External/Bioinformatics/Mouse/Annotation/2017Aug/refSeqGenes_mm10_23Aug2017.gtf | 
sort -k1,1 -k4,4n >
/storage/scic/Data/External/Bioinformatics/Mouse/Annotation/2017Aug/refSeqGenes_mm10_Aug2017.gtf
```
One needs to add the ERCC to the annotation.

```
awk -F"\t" 'OFS="\t"{print $1,"custom","exon",1,length($5),".","+",".","gene_id \42"$1"\42; transcript_id \42"$2"\42;"}' 
/storage/scic/Data/External/Bioinformatics/Mouse/Annotation/2017Aug/ERCCSequences.txt |
sort -k1,1 -k4,4n > 
/storage/scic/Data/External/Bioinformatics/Mouse/Annotation/2017Aug/ERCCSequences.gtf
```

Concatenate ERCC annotation with RefSeq annotation.

```
sort -k1,1 -k4,4n
/storage/scic/Data/External/Bioinformatics/Mouse/Annotation/2017Aug/ERCCSequences.gtf
/storage/scic/Data/External/Bioinformatics/Mouse/Annotation/2017Aug/refSeqGenes_mm10_Aug2017.gtf
> /storage/scic/Data/External/Bioinformatics/Mouse/Annotation/2017Aug/refSeqGenes_mm10_withERCC_Aug2017.gtf
```
#### count gene expression
Number of reads per gene is counted using [featureCounts](http://bioinf.wehi.edu.au/featureCounts/).

```
featureCounts 
-T 8 -t exon -g gene_id -Q 255
-a /storage/scic/Data/External/Bioinformatics/Mouse/Annotation/2017Aug/refSeqGenes_mm10_withERCC_Aug2017.gtf 
-o /storage/schu/ProjectGroups/Ribosome/IPRiboTag/counts/counts.txt 
$(ls /storage/schu/ProjectGroups/Ribosome/IPRiboTag/bams/*.bam)
```

### Data summary
Data summary is [generated](./scripts/experimentSummary.pl):

```
perl experimentSummary.pl
/storage/schu/ProjectGroups/Ribosome/IPRiboTag/logs/
/storage/schu/ProjectGroups/Ribosome/IPRiboTag/counts/counts.txt
> /storage/schu/ProjectGroups/Ribosome/IPRiboTag/counts/summaryTable_25Aug2017.txt
```

|sample      |raw.reads|unq.aligned.reads|annotated.reads|ercc.reads|unq.aligned.%|annotated.%|ercc.%  |
|:-----------|:-------:|:---------------:|:-------------:|:--------:|:-----------:|:---------:|:------:|
|IP-N1       |35845847 |30979079         |27616326       |111607    |86.423063    |77.041912  |0.311353|
|IP-N2       |53512771 |50134207         |44976182       |293686    |93.686434    |84.047567  |0.548815|
|IP-S1       |40982464 |37904183         |31788957       |1970701   |92.488785    |77.567218  |4.808644|
|IP-S2       |38774894 |36344793         |30498401       |2162340   |93.732798    |78.655021  |5.576650|
|Input-N1    |41731189 |36018085         |28845523       |23367     |86.309750    |69.122217  |0.055994|
|Input-N2    |41325268 |35106845         |27786275       |23154     |84.952492    |67.237979  |0.056029|
|Input-S1    |41166235 |37763640         |32799785       |402738    |91.734500    |79.676427  |0.978321|
|Input-S2    |39777825 |36254744         |31240238       |405402    |91.143103    |78.536818  |1.019166|
|Undetermined|9423303  |5301158          |4499530        |78246     |56.255837    |47.748969  |0.830346|

|Reads summary|
|:-------------------------:|
|![Reads summary](./figures/figureX_ReadsSummary.png)|


### Sample correlation

|Genes|ERCC|
|:-------------------------:|:---------------------------:|
|![Gene correlation](./figures/figureX_SampleCorrelation_Genes.png)|![ERCC correlation](./figures/figureX_SampleCorrelation_ERCC.png)|


### Normalisation factors

|Compare normalisation factors|
|:---------------------------:|
|![norm factors](./figures/figureX_NormalisationFactors.png)|


### Differential expression

Legend
* black dots are ERCC counts
* red circles are Ribosomal proteins


|Somata Layer (Input vs. IP)|Neuropil Layer (Input vs. IP)|
|:-------------------------:|:---------------------------:|
|![MA Somata](./figures/figureMA_Somata-INvIP.png "Somata Layer (Input vs. IP)")|![MA Neuropil](./figures/figureMA_Neuropil-INvIP.png "Neuropil Layer (Input vs. IP)")|


|Input Sample (Somata vs. Neuropil)|IP Sample (Somata vs. Neuropil)|
|:-------------------------:|:---------------------------:|
|![MA Somata](./figures/figureMA_Input-SvN.png "Input Sample (Somata vs. Neuropil)")|![MA Neuropil](./figures/figureMA_IP-SvN.png "IP Sample (Somata vs. Neuropil)")|
