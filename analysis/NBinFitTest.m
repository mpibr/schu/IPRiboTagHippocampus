function [stats] = NBinFitTest(X, Y, varargin)

    %% parse Name-Value pairs
    pobj = inputParser;
    addParameter(pobj, 'Alpha', 0.05, @isnumeric);
    addParameter(pobj, 'FoldChange', 0, @isnumeric);
    addParameter(pobj, 'MinExpression', 1, @isnumeric);
    addParameter(pobj, 'PlotMA', false, @islogical);
    addParameter(pobj, 'LabelUp', 'purple', @ischar);
    addParameter(pobj, 'LabelDown', 'blue', @ischar);
    addParameter(pobj, 'ColorUp', [148,0,211]./255, @isnumeric);
    addParameter(pobj, 'ColorDown', [30,144,255]./255, @isnumeric);
    addParameter(pobj, 'VarianceLink', 'LocalRegression', @ischar);
    parse(pobj, varargin{:});

    %% assign
    thresh_pv = pobj.Results.Alpha;
    thresh_m = pobj.Results.FoldChange;
    thresh_a = pobj.Results.MinExpression;
    plot_ma = pobj.Results.PlotMA;
    label_up = pobj.Results.LabelUp;
    label_dn = pobj.Results.LabelDown;
    color_up = pobj.Results.ColorUp;
    color_dn = pobj.Results.ColorDown;
    vartest = pobj.Results.VarianceLink;
    
    %% calculate MA components
    nrm = DESeqNormalization([X,Y]);
    Xnrm = nrm(:,1:size(X,2));
    Ynrm = nrm(:,(size(X,2)+1):end);
    A = log10(mean(nrm, 2));
    M = log2(mean(Ynrm,2)) - log2(mean(Xnrm,2));
    
    %% execute test
    t = nbintest(X, Y, 'VarianceLink', vartest);
    
    %% calculate false-discovery rate
    idx_pass = t.pValue <= thresh_pv;
    
    %% set thresholds
    thresh_m_down = median(M) - log2(thresh_m);
    thresh_m_up = median(M) + log2(thresh_m);
    
    idx_up = idx_pass & (M >= thresh_m_up) & (A >= 
    
    
%{

    
    
    
    % calculate false discovery rate
    % Benjamini-Hochberg procedure
    p_raw = t.pValue;
    p_raw(p_raw == 0) = eps;
    [p_raw, idx_sort] = sort(p_raw,'ascend');
    idx_rev(idx_sort) = (1:size(idx_sort,1))';

    siz = size(p_raw,1);
    p_bh = thresh_pv .* (1:siz)' ./ siz;
    p_adj = (siz./(1:siz)').*p_raw;
    % adjusted p-values can be greater than 1 !!! do not correct for this
    %p_adj = p_adj ./ max(p_adj);
    idx_pass = p_raw <= p_bh;
    idx_pass = idx_pass(idx_rev);
    p_adj = p_adj(idx_rev);
    p_raw = p_raw(idx_rev);
    
    thresh_fc_down = median(M) - log2(thresh_fc);
    thresh_fc_up = median(M) + log2(thresh_fc);
    
    idx_x = idx_pass & (M <= thresh_fc_down);
    idx_y = idx_pass & (M >= thresh_fc_up);
    idx_none = ~(idx_x | idx_y);

    
    %% --- MA Plot --- %%
    if plot_ma
        
        figure('Color','w');
        hold(gca,'on');
        plot(A(idx_none),M(idx_none), '.', 'color', [.65,.65,.65]);
        plot(A(idx_x), M(idx_x), '.', 'color', [30,144,255]./255,'markersize',10);
        plot(A(idx_y), M(idx_y), '.', 'color', [148,0,211]./255,'markersize',10);
        plot([min(A),max(A)],[median(M),median(M)],'k');
        hold(gca,'off');
        
        y_lim = max(abs(M)) + 1;
        y_lim = [-y_lim, y_lim];
        x_lim = [min(A)-0.5, max(A)+0.5];
        set(gca, 'box','off',...
                 'xlim', x_lim,...
                 'ylim', y_lim);
        xlabel('average expression [log_1_0]','fontsize',10);
        ylabel('fold change [log_2]', 'fontsize',10);
        
    end
    
    %% --- Vulcano Plot --- %%
    if plot_vulcano
        
        X = M;
        Y = -log10(p_adj);
        x_lim = max(abs(X)) + 1;
        x_lim = [-x_lim, x_lim];
        y_lim = [0,min([max(Y),20])];
        
        figure('color','w');
        hold on;
        plot(X(idx_none),Y(idx_none),'.','color',[.65,.65,.65]);
        plot(X(idx_x), Y(idx_x), '.', 'color', [30,144,255]./255,'markersize',10);
        plot(X(idx_y), Y(idx_y), '.', 'color', [148,0,211]./255,'markersize',10);
        plot([median(X),median(X)],[0,max(Y)],'k');
        plot([median(X)-log2(thresh_fc),median(X)-log2(thresh_fc)],[0,max(Y)],'k');
        plot([median(X)+log2(thresh_fc),median(X)+log2(thresh_fc)],[0,max(Y)],'k');
        plot([min(X),max(X)],[-log10(thresh_pv),-log10(thresh_pv)],'k');
        hold off;
        set(gca,'box','off',...
                'xlim', x_lim,...
                'ylim', y_lim);
        xlabel('fold change [log_2]','fontsize',10);
        ylabel('adjusted pValue [-log_1_0]','fontsize',10);
    end
    
    % pack in structure
    stats.nrm = nrm;
    stats.A = A;
    stats.M = M;
    stats.pval = p_raw;
    stats.padj = p_adj;
    stats.idx_x = idx_x;
    stats.idx_y = idx_y;
    stats.idx_none = idx_none;
    stats.t = t;
%}    
end

