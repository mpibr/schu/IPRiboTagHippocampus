% normalizeCounts
clc
clear variables
close all

%% read table
fr = fopen('tableCounts_LocalTranslatome_07Oct2017.txt', 'r');
hdr = fgetl(fr);
hdr = regexp(hdr,'\t','split');
fmt = repmat({'%n'},length(hdr), 1);
fmt(1) = {'%s'};
fmt = sprintf('%s ', fmt{:});
hdr = hdr(2:end-1);
txt = textscan(fr, fmt, 'delimiter','\t');
fclose(fr);
sym = txt{1};
counts = [txt{2:end-1}];


%% calculate norm factor
idxSort = [3,4,1,2,7,8,5,6];
counts = counts(:,idxSort);

libsize = sum(counts);
rpm = bsxfun(@rdivide, 1e6.*counts,libsize);

idxFilter = all(rpm < 2, 2);
counts(idxFilter,:) = [];


[nrm, fctr] = DESeqNormalization(counts);
tmm = [1.0484112, 1.0392067, 1.1009704, 1.1138345, 0.9064669, 0.9040365, 0.9777677, 0.9341066];
rle = [1.0538588, 1.0668648, 1.0575540, 1.0870879, 0.9120438, 0.9179667, 0.9913327, 0.9321352]; 
%tmp = lsiz .* fctr;


tLocal = nbintest(counts(:,1:2),counts(:,5:6),'VarianceLink','Identity');

%{
%nrm = bsxfun(@times, counts, fctr);

figure('color','w');
hold on;
plot(fctr, 'k');
plot(tmm,'g');
plot(rle,'r');
plot(libsize./mean(libsize), 'b');
plot(median(nrm)./mean(median(nrm)),'c');
hold off;
%}