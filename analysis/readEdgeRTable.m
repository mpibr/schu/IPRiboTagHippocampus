function [stats] = readEdgeRTable(file_name, varargin)
% read EdgeR results table

    %% parse Name-Value pairs
    pobj = inputParser;
    addParameter(pobj, 'Alpha', 0.05, @isnumeric);
    addParameter(pobj, 'FoldChange', 1, @isnumeric);
    addParameter(pobj, 'MinCPM', 0, @isnumeric);
    addParameter(pobj, 'XLim', [], @isnumeric);
    addParameter(pobj, 'YLim', [], @isnumeric);
    addParameter(pobj, 'PlotMA', false, @islogical);
    addParameter(pobj, 'LabelUp', 'purple', @ischar);
    addParameter(pobj, 'LabelDown', 'blue', @ischar);
    addParameter(pobj, 'ColorUp', [148,0,211]./255, @isnumeric);
    addParameter(pobj, 'ColorDown', [30,144,255]./255, @isnumeric);
    addParameter(pobj, 'QueryLabel', [], @islogical);
    addParameter(pobj, 'Exclude', [], @ischar);
    addParameter(pobj, 'QueryList', [], @iscell);
    addParameter(pobj, 'QueryColor', [], @isnumeric);
    parse(pobj, varargin{:});
    
    %% assign
    thresh_pv = pobj.Results.Alpha;
    thresh_fc = pobj.Results.FoldChange;
    thresh_cpm = pobj.Results.MinCPM;
    x_lim = pobj.Results.XLim;
    y_lim = pobj.Results.YLim;
    plot_ma = pobj.Results.PlotMA;
    label_up = pobj.Results.LabelUp;
    label_dn = pobj.Results.LabelDown;
    color_up = pobj.Results.ColorUp;
    color_dn = pobj.Results.ColorDown;
    query_label = pobj.Results.QueryLabel;
    label_exclude = pobj.Results.Exclude;
    queryList = pobj.Results.QueryList;
    queryColor = pobj.Results.QueryColor;
    
    fr = fopen(file_name, 'r');
    fgetl(fr);
    txt = textscan(fr, '%s %n %n %n %n %n', 'delimiter', '\t');
    fclose(fr);

    stats.id = txt{1};
    stats.logFC = txt{2};
    stats.logCPM = txt{3};
    stats.F = txt{4};
    stats.PValue = txt{5};
    stats.FDR = txt{6};
    indexExclude = strncmp(label_exclude, stats.id, length(label_exclude));
    stats.idx_down = (stats.FDR <= thresh_pv) & (stats.logFC <= (median(stats.logFC) - log2(thresh_fc))) & (stats.logCPM >= thresh_cpm) & ~indexExclude;
    stats.idx_up = (stats.FDR <= thresh_pv) & (stats.logFC >= (median(stats.logFC) + log2(thresh_fc))) & (stats.logCPM >= thresh_cpm) & ~indexExclude;
    stats.idx_none = ~(stats.idx_down | stats.idx_up) & ~indexExclude;
    
    %% --- MA Plot --- %%
    if plot_ma
        
        A = stats.logCPM;
        M = stats.logFC;
        idx_none = stats.idx_none;
        idx_up = stats.idx_up;
        idx_down = stats.idx_down;
        
        figure('Color','w');
        hold(gca,'on');
        plot(A(idx_none),M(idx_none), '.', 'color', [144,164,174]./255, 'markersize', 2);
        h = plot(A(idx_down), M(idx_down), '.', 'color', color_dn, 'markersize',5);
        h.Color(4) = 0.1;
        h = plot(A(idx_up), M(idx_up), '.', 'color', color_up, 'markersize',5);
        h.Color(4) = 0.1;
        
        if ~isempty(query_label)
            plot(A(query_label),M(query_label), 'r.','markersize', 12);
        end
        
        if ~isempty(queryList)
            
            h = zeros(length(queryList), 1);
            for l = 1 : length(queryList)
                idxQuery = strcmp(queryList{l}, stats.id);
                h(l) = plot(A(idxQuery), M(idxQuery), '.', 'markersize', 25, 'color',queryColor(l,:));
                
            end
            hl = legend(h,queryList);
            set(hl,'edgecolor','w','location','northeast');
            
        end
        
        plot([min(A),max(A)],[median(M),median(M)],'k');
        hold(gca,'off');
        
        if isempty(y_lim)
            y_lim = max(abs(M)) + 1;
            y_lim = [-y_lim, y_lim];
        end
        
        if isempty(x_lim)
            x_lim = [min(A)-0.5, max(A)+0.5];
        end
        text_offset = 0.05*max(y_lim);
        text(x_lim(1)+text_offset,y_lim(1)+text_offset,...
            sprintf('%s: %d', label_dn, sum(idx_down)),...
            'fontsize',10,...
            'verticalalignment','bottom',...
            'horizontalalignment','left');
        
        text(x_lim(1)+text_offset,y_lim(2)-text_offset,...
            sprintf('%s: %d', label_up, sum(idx_up)),...
            'fontsize',10,...
            'verticalalignment','top',...
            'horizontalalignment','left');
        
        set(gca, 'box','off',...
                 'xlim', x_lim,...
                 'ylim', y_lim,...
                 'fontsize',12);
        xlabel('average expression [log_2 CPM]','fontsize',14);
        ylabel('fold change [log_2]', 'fontsize',14);
        
    end

end

