function summary(fileSummary, varargin)
% static method SUMMARY
% class IPRiboTag
%
% plots counts summary file
%
% Aug 2017
% georgi.tushev@brain.mpg.de
%

    %% parse input
    pobj = inputParser;
    addRequired(pobj, 'filename', @ischar);
    addParameter(pobj, 'Export', [], @ischar);
    parse(pobj, fileSummary, varargin{:});
    fileExport = pobj.Results.Export;
    
    %% create file handle
    fh = fopen(fileSummary, 'r');
    fgetl(fh);
    txt = textscan(fh, '%*s %n %n %n  %n %*s %*s %*s', 'delimiter', '\t');
    fclose(fh);
    
    %% create plot structures
    counts = [txt{1:end}]./1e6;
    ncounts = bsxfun(@rdivide, counts, counts(:,1));
    
    %% create figure
    hfig = figure('color','w','menubar','none','toolbar','none','numbertitle','off','name','summary');
    hspread = plotSpread(counts);
    hAxes = hspread{3};
    hChildren = get(hAxes, 'Children');
    colorCount = 200;
    colorMap = colormap(jet(colorCount));
    colorIndex = ceil(ncounts ./ (1/colorCount));
    colorIndex = fliplr(colorIndex);
    xValue = get(hChildren, 'XData');
    yValue = get(hChildren, 'YData');
    set(hChildren, 'Color', [1, 1, 1]);
    hold(hAxes, 'on');
    for k = 1 : size(counts, 2)
        scatter(xValue{k}, yValue{k}, 12, colorMap(colorIndex(:,k), :), 'filled');
    end
    hold(hAxes, 'off');
    
    hc = colorbar();
    set(hc, 'ticks', (0:.1:1),...
            'ticklabels', (0:10:100));
    ylabel(hc, 'relative fraction [%]', 'fontsize', 12);    
    
    
    set(hAxes,'box','off',...
            'xlim',[.5,size(counts, 2) + 0.5],...
            'xticklabel',{'sequenced','aligned','genes','ercc'},...
            'xticklabelrotation',45,...
            'fontsize',12);
    ylabel(hAxes, ' reads [millions]', 'fontsize', 12);    
    
    %% export figure
    if ~isempty(fileExport)
        print(hfig, '-dpng', '-r300', [fileExport,'.png']);
        print(hfig, '-painters', '-dsvg', '-r300', [fileExport,'.svg']);
    end

end

