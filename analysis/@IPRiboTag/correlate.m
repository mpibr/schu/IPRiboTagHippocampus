function obj = correlate(obj, varargin)
% method CORRELATE
% class IPRiboTag
%
% plots object counts correlations
%
% require
% GUI Layout toolbox
% https://de.mathworks.com/matlabcentral/fileexchange/47982-gui-layout-toolbox
%
% Aug 2017
% georgi.tushev@brain.mpg.de
%

    %% parse input
    pobj = inputParser;
    addParameter(pobj, 'Export', [], @ischar);
    addParameter(pobj, 'IndexSamples', (1:size(obj.counts, 2))', @islogical);
    addParameter(pobj, 'IndexRecords', (1:size(obj.counts, 1))', @islogical);
    addParameter(pobj, 'FontCorrelation', 10, @isnumeric);
    addParameter(pobj, 'FontLabels', 10, @isnumeric);
    addParameter(pobj, 'CorrelationType', 'Spearman', @ischar);
    addParameter(pobj, 'Bins', 32, @isnumeric);
    parse(pobj, varargin{:});
    
    fileExport = pobj.Results.Export;
    indexSamples = pobj.Results.IndexSamples;
    indexRecords = pobj.Results.IndexRecords;
    fontCorrelation = pobj.Results.FontCorrelation;
    fontLabels = pobj.Results.FontLabels;
    correlationType = pobj.Results.CorrelationType;
    histogramBins = pobj.Results.Bins;
    
    %% counts and samples
    counts = obj.counts(indexRecords, indexSamples);
    samples = obj.samples(indexSamples);
    nsamples = length(samples);
    
    %% index matrix
    mtrxTriL = tril(true(nsamples, nsamples));
    mtrxTriU = triu(true(nsamples, nsamples));
    mtrxDiag = mtrxTriL & mtrxTriU;
    mtrxTriL(mtrxDiag) = false;
    mtrxTriU(mtrxDiag) = false;
    
    %% create figure
    hFigure = figure(...
        'Color', 'w',...
        'Name', 'sample correlations',...
        'NumberTitle', 'off',...
        'MenuBar', 'none',...
        'ToolBar', 'none',...
        'Position', [1, 1, 640, 640]);
    movegui(hFigure, 'north');
    
    %% add layout
    hLayout = uix.Grid(...
        'Parent', hFigure,...
        'Spacing', 5,...
        'Padding', 20);
    
    %% loop through samples
    for i = 1 : nsamples
        for j = 1 : nsamples
            
            %%% current data vectors
            x = log2(counts(:,i)+1);
            y = log2(counts(:,j)+1);
            axesLimit = ceil(max([max(x),max(y)]));
            
            %%% create axes
            hAxes = axes('Parent', hLayout,...
                         'Box', 'on',...
                         'XTick',[],...
                         'YTick',[],...
                         'Units', 'normalized');
            
            %%% diagonal
            if mtrxDiag(i, j)
                [n, edges] = histcounts(x, histogramBins);
                bar(hAxes, edges(2:end), n./max(n), 0.2,'EdgeColor','none','FaceColor',[.4,.4,.4]);
                set(hAxes,'Box','off','Layer','top',...
                       'XTick',[],'XLim',[0, axesLimit],...
                       'YTick',[],'YLim',[0,1]);
            end
            
            %%% upper triangular
            if mtrxTriU(i, j)
                rsq = corr(x,y, 'Type',correlationType);
                text(0.5,0.5,sprintf('%.4f',rsq),'Parent',hAxes,...
                'VerticalAlignment','middle','HorizontalAlignment','center',...
                'FontSize',fontCorrelation);
                set(hAxes,'XTick',[],'XLim',[0,1],'YTick',[],'YLim',[0,1]);
            end
            
            %%% lower triangular
            if mtrxTriL(i, j)
                
                hold(hAxes,'on');
                plot(x,y,'.','Color',[.8,.8,.8]);
                plot([0,axesLimit],[0,axesLimit],'k');
                hold(hAxes,'off');
                set(hAxes,'Box','off','Layer','top',...
                       'XTick',[],'XLim',[-2,axesLimit],...
                       'YTick',[],'YLim',[-2,axesLimit]);
            end
            
            %%% add X-Label
            if(j == nsamples)
                
                xlimNow = get(hAxes, 'XLim');
                xPoint = xlimNow(1) + 0.5*diff(xlimNow);
                text(xPoint, 0, samples{i},...
                    'VerticalAlignment','top',...
                    'HorizontalAlignment','center',...
                    'FontSize', fontLabels,...
                    'Parent', hAxes);
                
            end
            
            
            %%% add Y-Label
            if(i == 1)
                
                ylimNow = get(hAxes, 'YLim');
                yPoint = ylimNow(1) + 0.5*diff(ylimNow);
                text(0, yPoint, samples{j},...
                    'VerticalAlignment','bottom',...
                    'HorizontalAlignment','center',...
                    'Rotation', 90,...
                    'FontSize', fontLabels,...
                    'Parent', hAxes);
            end
            
        end
    end
    
    %% fix grid size
    set(hLayout, 'Heights', -ones(1,nsamples), 'Widths', -ones(1, nsamples));
    
    %% export
    if ~isempty(fileExport)
        print(hFigure, '-dpng', '-r300', [fileExport,'.png']);
        %print(hFigure, '-painters', '-dsvg', '-r300', [fileExport,'.svg']);
    end
end

