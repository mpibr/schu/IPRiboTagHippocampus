function obj = de(obj, xtoken, ytoken, varargin)
% method DE
% class IPRiboTag
%
% plots differential expression
%
% Aug 2017
% georgi.tushev@brain.mpg.de
%

    %% parse input
    pobj = inputParser;
    addRequired(pobj, 'xtoken', @ischar);
    addRequired(pobj, 'ytoken', @ischar);
    %addParameter(pobj, 'xWeight', 1, @isnumeric);
    %addParameter(pobj, 'yWeight', 1, @isnumeric);
    addParameter(pobj, 'Alpha', 0.05, @isnumeric);
    addParameter(pobj, 'FoldChange', 1, @isnumeric);
    addParameter(pobj, 'MinExpression', 1, @isnumeric);
    addParameter(pobj, 'PlotMA', false, @islogical);
    addParameter(pobj, 'PlotVulcano', false, @islogical);
    addParameter(pobj, 'LabelUp', 'purple', @ischar);
    addParameter(pobj, 'LabelDown', 'blue', @ischar);
    addParameter(pobj, 'ColorUp', [148,0,211]./255, @isnumeric);
    addParameter(pobj, 'ColorDown', [30,144,255]./255, @isnumeric);
    addParameter(pobj, 'VarianceLink', 'LocalRegression', @ischar);
    addParameter(pobj, 'XLim', [], @isnumeric);
    addParameter(pobj, 'YLim', [], @isnumeric);
    addParameter(pobj, 'Export', [], @ischar);
    addParameter(pobj, 'Query', [], @islogical);
    addParameter(pobj, 'QueryList', [], @iscell);
    addParameter(pobj, 'QueryColor', [], @isnumeric);
    addParameter(pobj, 'FilterRows', false(size(obj.counts,1),1), @islogical);
    parse(pobj, xtoken, ytoken, varargin{:});
    
    %% assign
    threshPValue = pobj.Results.Alpha;
    threshM = pobj.Results.FoldChange;
    threshA = pobj.Results.MinExpression;
    plotMA = pobj.Results.PlotMA;
    plotVulcano = pobj.Results.PlotVulcano;
    labelUp = pobj.Results.LabelUp;
    labelDown = pobj.Results.LabelDown;
    colorUp = pobj.Results.ColorUp;
    colorDown = pobj.Results.ColorDown;
    vartest = pobj.Results.VarianceLink;
    xLimit = pobj.Results.XLim;
    yLimit = pobj.Results.YLim;
    fileExport = pobj.Results.Export;
    indexQuery = pobj.Results.Query;
    indexFilterRows = pobj.Results.FilterRows;
    queryList = pobj.Results.QueryList;
    queryColor = pobj.Results.QueryColor;
    %xWeight = pobj.Results.xWeight;
    %yWeight = pobj.Results.yWeight;
    
    
    %% extract X and Y
    idxX = strncmp(xtoken, obj.samples, length(xtoken));
    X = obj.counts(:, idxX) + 1;
    idxY = strncmp(ytoken, obj.samples, length(ytoken));
    Y = obj.counts(:, idxY) + 1;
    
    %% calculate MA components
    rawCounts = [X,Y];
    
    %if isempty(obj.factors)
        indexFilterLow = all(bsxfun(@ge, rawCounts, prctile(rawCounts, 25)), 2);
        [~, sizeFactor] = DESeqNormalization(rawCounts(~indexFilterRows & indexFilterLow & obj.indexExpressed,:));
    %else
        %sizeFactor = [obj.factors(idxX); obj.factors(idxY)]';
    %end
    
    nrm = bsxfun(@rdivide, rawCounts, sizeFactor);
    %[~, sizeFactor] = DESeqNormalization(ceil(nrm(~indexFilterRows,:)));
    %nrm = bsxfun(@rdivide, ceil(nrm), sizeFactor);
    
    Xnrm = nrm(:,1:size(X,2));
    Ynrm = nrm(:,(size(X,2)+1):end);
    A = log10(mean(nrm, 2));
    M = log2(mean(Ynrm,2)) - log2(mean(Xnrm,2));
    
    %% execute test
    sizeFactor = [{sizeFactor(1:sum(idxX))},{sizeFactor(sum(idxX)+1:end)}];
    t = nbintest(X, Y,...
        'VarianceLink', vartest,...
        'SizeFactor', sizeFactor);
    
    %% calculate false-discovery rate
    fdr = mafdr(t.pValue);
    
    %% set thresholds
    idx_expressed = obj.indexExpressed('Columns',idxX | idxY);
    idx_pass = fdr <= threshPValue;
    idx_a = A >= log10(threshA);
    idx_down = (M <= (median(M) - log2(threshM))) & idx_a & idx_pass & idx_expressed & ~indexFilterRows;
    idx_up = (M >= median(M) + log2(threshM)) & idx_a & idx_pass & idx_expressed & ~indexFilterRows;
    idx_none = ~(idx_up | idx_down) & idx_expressed & ~indexFilterRows;
    
    %% assign results
    obj.detest.A = A;
    obj.detest.M = M;
    obj.detest.indexUp = idx_up;
    obj.detest.indexDown = idx_down;
    obj.detest.indexNone = idx_none;
    obj.detest.stats = t;
    obj.detest.fdr = fdr;
    
    %% MA Plot
    if plotMA
        
        hFigure = figure(...
            'Color','w',...
            'Name', 'MA Plot',...
            'NumberTitle', 'off',...
            'MenuBar', 'none',...
            'ToolBar', 'none');
        movegui(hFigure, 'north');
        
        if isempty(yLimit)
            yLimit = ceil(max(abs(M))+0.5);
            yLimit = [-yLimit, yLimit];
        end
        
        if isempty(xLimit)
            xLimit = [min(A)-0.5, max(A)+0.5];
        end
        
        
        hold(gca,'on');
        %plot(xLimit, [0,0],'-','color',[.85,.85,.85]);
        plot(A(idx_none),M(idx_none), '.', 'color', [.65,.65,.65],'markersize',3);
        plot(A(idx_down), M(idx_down), '.', 'color', colorDown,'markersize',5);
        plot(A(idx_up), M(idx_up), '.', 'color', colorUp,'markersize',5);
        
        %{
        if any(indexFilterRows)
            plot(A(indexFilterRows), M(indexFilterRows), 'k.', 'markersize', 6);
        end
        %}
        
        
        if ~isempty(indexQuery)
            plot(A(indexQuery), M(indexQuery), 'ro', 'markersize', 5);
        end
        
        
        if ~isempty(queryList)
            
            h = zeros(length(queryList), 1);
            for l = 1 : length(queryList)
                idxQuery = strcmp(queryList{l}, obj.genes);
                h(l) = plot(A(idxQuery), M(idxQuery), '.', 'markersize', 25, 'color',queryColor(l,:));
                
            end
            hl = legend(h,queryList);
            set(hl,'edgecolor','w','location','northeast');
            
        end
        %}
        
        plot([min(A),max(A)],[median(M),median(M)],'k');
        hold(gca,'off');
        
        
        
        text_offset = 0.05*max(yLimit);
        text(xLimit(1)+text_offset,yLimit(1)+text_offset,...
            sprintf('%s: %d', labelDown, sum(idx_down)),...
            'fontsize',10,...
            'verticalalignment','bottom',...
            'horizontalalignment','left');
        
        text(xLimit(1)+text_offset,yLimit(2)-text_offset,...
            sprintf('%s: %d', labelUp, sum(idx_up)),...
            'fontsize',10,...
            'verticalalignment','top',...
            'horizontalalignment','left');
        
        set(gca, 'box','off',...
                 'xlim', xLimit,...
                 'ylim', yLimit,...
                 'fontsize',12);
        xlabel('average expression [log_1_0]', 'fontsize', 14);
        ylabel('fold change [log_2]', 'fontsize', 14);
        
    end
    
    %% export
    if ~isempty(fileExport)
        print(hFigure, '-dpng', '-r300', [fileExport,'.png']);
        %print(hFigure, '-painters', '-dsvg', '-r300', [fileExport,'.svg']);
    end
    
    %% Vulcano Plot
    
    if plotVulcano
        
        X = M;
        Y = -log10(fdr);
        xLimit = max(abs(X)) + 1;
        xLimit = [-xLimit, xLimit];
        yLimit = [0,min([max(Y),12])];
        
        figure('color','w');
        hold on;
        plot(X(idx_none),Y(idx_none),'.','color',[.65,.65,.65]);
        plot(X(idx_down), Y(idx_down), '.', 'color', [30,144,255]./255,'markersize',10);
        plot(X(idx_up), Y(idx_up), '.', 'color', [148,0,211]./255,'markersize',10);
        plot([median(X),median(X)],[0,max(Y)],'k');
        plot([median(X)-log2(threshM),median(X)-log2(threshM)],[0,max(Y)],'k');
        plot([median(X)+log2(threshM),median(X)+log2(threshM)],[0,max(Y)],'k');
        plot([min(X),max(X)],[-log10(threshPValue),-log10(threshPValue)],'k');
        hold off;
        set(gca,'box','off',...
                'xlim', xLimit,...
                'ylim', yLimit);
            
        text_offset = 0.05*max(xLimit);
        text(xLimit(1)+text_offset,-log10(threshPValue)+text_offset,...
            sprintf('%s: %d', labelDown, sum(idx_down)),...
            'fontsize',10,...
            'verticalalignment','bottom',...
            'horizontalalignment','left');
        
        text(xLimit(2)+text_offset,-log10(threshPValue)+text_offset,...
            sprintf('%s: %d', labelUp, sum(idx_up)),...
            'fontsize',10,...
            'verticalalignment','bottom',...
            'horizontalalignment','right');    
            
        xlabel('fold change [log_2]','fontsize',10);
        ylabel('adjusted pValue [-log_1_0]','fontsize',10);
    end
    
    
end

