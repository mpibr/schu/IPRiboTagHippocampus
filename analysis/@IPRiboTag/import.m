function [ obj ] = import(obj, filename)
% method IMPORT
% class IPRiboTag
%
% imports featureCount output table
%
% Aug 2017
% georgi.tushev@brain.mpg.de
%

    %% create file handle
    fh = fopen(filename, 'r');

    %% read header
    hdr = fgetl(fh);
    while hdr(1) == '#'
        hdr = fgetl(fh);
    end
    hdr = regexp(hdr, '\t', 'split');
    ncols = length(hdr);

    %% parse sample ids
    hdr = hdr(7:end);
    hdr = regexp(hdr, '\/','split');
    hdr = cellfun(@(x) x(end), hdr)';
    hdr = regexp(hdr, '\_','split');
    hdr = cellfun(@(x) x(1), hdr);

    %% set read format
    fmt = repmat({'%n'}, ncols, 1);
    fmt(2:5) = {'%*s'};
    fmt(1) = {'%s'};
    fmt = sprintf('%s ',fmt{:});
    fmt(end) = [];

    %% parse data
    txt = textscan(fh, fmt, 'delimiter','\t');

    %% close file handle
    fclose(fh);

    %% set class properties
    obj.samples = hdr;
    obj.genes = txt{1};
    obj.span = txt{2};
    obj.counts = [txt{3:end}];
    
    %% filter empty counts
    indexFilter = all(obj.counts == 0, 2);
    obj.genes(indexFilter) = [];
    obj.span(indexFilter) = [];
    obj.counts(indexFilter, :) = [];

end

