function idx = indexExpressed(obj, varargin)
% method CORRELATE
% class IPRiboTag
%
% plots object counts correlations
%
% require
% GUI Layout toolbox
% https://de.mathworks.com/matlabcentral/fileexchange/47982-gui-layout-toolbox
%
% Aug 2017
% georgi.tushev@brain.mpg.de
%

    %% parse input
    pobj = inputParser;
    addParameter(pobj, 'RPM', 2, @isnumeric);
    addParameter(pobj, 'Columns', true(size(obj.counts, 2), 1), @islogical);
    addParameter(pobj, 'Rows', false(size(obj.counts, 1), 1), @islogical);
    parse(pobj, varargin{:});

    %% assign input        
    threshRPM = pobj.Results.RPM;
    indexColumn = pobj.Results.Columns;
    indexRows = pobj.Results.Rows;
    
    %% calculate RPM normalization
    countsRPM = bsxfun(@rdivide,...
               1e6 .* obj.counts(:, indexColumn),...
               sum(obj.counts(~indexRows, indexColumn)));
           
   %% index logic
   idx = any(countsRPM >= threshRPM, 2);
   idx(indexRows) =  false;
        
end

