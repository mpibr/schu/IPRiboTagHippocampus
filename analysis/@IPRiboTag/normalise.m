function obj = normalise(obj, sampleConcentration, sampleDilution, fileSpikedIns, varargin)
% method DE
% class IPRiboTag
%
% calculates size factors
% based on spiked-ins linear regression
%
% Aug 2017
% georgi.tushev@brain.mpg.de
%

    %% parse input
    pobj = inputParser;
    addRequired(pobj, 'sampleConcentration', @isnumeric);
    addRequired(pobj, 'sampleDilution', @isnumeric);
    addRequired(pobj, 'fileSpikedIns', @ischar);
    addParameter(pobj, 'Threshold', 3, @isnumeric);
    parse(pobj, sampleConcentration, sampleDilution, fileSpikedIns, varargin{:});
    threshold = pobj.Results.Threshold;
    
    %% calculate sample weight
    sampleWeight = sampleConcentration ./ sampleDilution;

    %% import spiked-ins metadata
    fr = fopen(fileSpikedIns,'r');
    fgetl(fr);
    txt = textscan(fr, '%*s %s %*s %n %n %*s %*s','delimiter', '\t');
    fclose(fr);
    erccQuery = txt{1};
    erccMix1 = txt{2};
    %erccMix2 = txt{3};
    
    %% extract spike-ins counts
    erccCounts = obj.counts(obj.indexErcc, :);
    erccLabel = obj.genes(obj.indexErcc);

    %% fix concentration to label
    [~,idxRef,idxQry] = intersect(erccLabel, erccQuery);
    erccConcentration(idxRef,1) = erccMix1(idxQry);

    
    %% calculate linear regression
    bArray = zeros(size(erccCounts, 2)-1, 2);
    for k = 1 : size(erccCounts, 2)-1

        X = erccConcentration .* sampleWeight(1);
        Y = erccCounts(:,k) + 1;
        pX = log10(X);
        pY = log10(Y);

        idxFit = Y > threshold;
        b = polyfit(pX(idxFit),pY(idxFit), 1);

        bArray(k,:) = b;
    end

    fctrErcc = bsxfun(@rdivide, bArray, mean(bArray));
    obj.factors = fctrErcc(:,1);
    
    
end

