function obj = lists(obj, fileDown, fileUp, fileBackground)
% method DE
% class IPRiboTag
%
% plots differential expression
%
% Aug 2017
% georgi.tushev@brain.mpg.de
%
    %% export background
    indexBackground = obj.detest.indexDown | obj.detest.indexUp | obj.detest.indexNone;
    out = [obj.genes(indexBackground),num2cell([obj.detest.A(indexBackground), obj.detest.M(indexBackground)])]';
    fw = fopen(fileBackground, 'w');
    fprintf(fw,'# gene\tavg.expression[log10]\tfold.change[log2]\n');
    fprintf(fw,'%s\t%.4f\t%.4f\n',out{:});
    fclose(fw);
    
    
    %% export down-regulated
    indexDown = obj.detest.indexDown;
    out = [obj.genes(indexDown),num2cell([obj.detest.A(indexDown), obj.detest.M(indexDown)])]';
    fw = fopen(fileDown, 'w');
    fprintf(fw,'# gene\tavg.expression[log10]\tfold.change[log2]\n');
    fprintf(fw,'%s\t%.4f\t%.4f\n',out{:});
    fclose(fw);
    
    %% export up-regulated
    indexUp = obj.detest.indexUp;
    out = [obj.genes(indexUp),num2cell([obj.detest.A(indexUp), obj.detest.M(indexUp)])]';
    fw = fopen(fileUp, 'w');
    fprintf(fw,'# gene\tavg.expression[log10]\tfold.change[log2]\n');
    fprintf(fw,'%s\t%.4f\t%.4f\n',out{:});
    fclose(fw);
    

end

