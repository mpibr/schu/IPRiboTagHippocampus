classdef IPRiboTag < handle
    % IPRiboTag
    % 
    % project cell-type specific IPRiboTag in hippocampus
    % integrated analysis
    %
    % Aug 2017
    % georgi.tushev@brain.mpg.de
    %
    
    properties
        
        samples
        genes
        counts
        span
        detest
        factors
        
    end
    
    properties (Dependent)
        
        indexGenes
        indexErcc
        
    end
    
    %% public methods
    methods
        
        function obj = IPRiboTag(filename)
            
            pobj = inputParser;
            addRequired(pobj, 'filename', @ischar);
            parse(pobj, filename);
            
            obj.import(filename);
            
        end
        
        obj = import(obj, filename);
        obj = correlate(obj, varargin);
        obj = de(obj, xtoken, ytoken, varargin);
        obj = indexExpressed(obj, varargin);
        obj = lists(obj, fileDown, fileUp, fileBackground);
        obj = normalise(obj, sampleConcentration, sampleDilution, fileSpikedIns, varargin);
        
    end
    
    %% methods associated with dependent properties
    methods
        
        function idx = get.indexErcc(obj)
            
            idx = strncmp('ERCC', obj.genes, 4);
            
        end
        
        function idx = get.indexGenes(obj)
            
            idx = ~obj.indexErcc;
            
        end
        
    end
    
    %% static methods
    methods (Static)
        
        summary(filename, varargin);
        
    end
    
end

