#!/usr/bin/perl

use warnings;
use strict;

sub parseReMapTable($$);
sub parseGTF($$);

MAIN:
{
    my $file_remap = shift;
    my $file_gtf = shift;
    my %table = ();
    
    parseReMapTable($file_remap, \%table);
    parseGTF($file_gtf, \%table);
    
    exit 0;
}

sub parseReMapTable($$)
{
    my $file_remap = $_[0];
    my $table_ref = $_[1];
    
    open(my $fh, "<", $file_remap) or die $!;
    while(<$fh>)
    {
        chomp($_);
        next if($_ =~ m/^#/);
        my ($symbol, $transcript) = split("\t", $_, 2);
        $table_ref->{$transcript} = $symbol;
    }
    close($fh);
    
}

sub parseGTF($$)
{
    my $file_gtf = $_[0];
    my $table_ref = $_[1];
    
    open(my $fh, "<", $file_gtf) or die $!;
    while(<$fh>)
    {
        chomp($_);
        next if($_ =~ m/^#/);
        my @gtf = split("\t", $_);
        my $ncols = scalar(@gtf);
        my $key = ($gtf[-1] =~ m/gene_id \"([NX][MR]\_[0-9]+)\"\;/) ? $1 : "<unknown>";
        my $symbol = exists($table_ref->{$key}) ? $table_ref->{$key} : "<unknown>";
        
        #print $key,"\t",$symbol,"\t",scalar(@gtf),"\n";
        print join("\t",@gtf[0..($ncols - 2)]),"\t",'gene_id "',$symbol,'"; transcript_id "',$key,'";',"\n";
        #print $key,"\n";
    }
    close($fh);
    
}


