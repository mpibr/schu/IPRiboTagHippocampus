#!/usr/bin/perl

use warnings;
use strict;

sub parseFeatureCountsTable($);

MAIN:
{
    my $file_counts = shift;
    
    parseFeatureCountsTable($file_counts);
    
    exit 0;
}

sub parseFeatureCountsTable($)
{
    my $file_counts = $_[0];
    
    open(my $fh, "<", $file_counts) or die $!;
    while(<$fh>)
    {
        chomp($_);
        next if ($_ =~ m/^#/);
        
        my ($record, @line) = split("\t", $_);
        splice(@line, 0, 5);
        
        if ($record eq "Geneid")
        {
            foreach(@line)
            {
                my $indexStart = rindex($_, '/') + 1;
                my $indexEnd = index($_, '_', $indexStart);
                my $name = substr($_, $indexStart, $indexEnd - $indexStart);
                $_ = $name;
            }
            print $record,"\t",join("\t",@line),"\n";
            
        }
        else
        {
            
            
            
            my $counts = 0;
            foreach(@line)
            {
                $counts += $_;
            }
            my $output = join("\t",@line);
            print $record,"\t",$output,"\n" if($counts > 0);
            
        }
            
    }
    close($fh);
    
}