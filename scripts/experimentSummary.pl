#!/usr/bin/perl

use warnings;
use strict;

sub parseLogPath($$);
sub parseLogFile($);
sub parseCounts($$);
sub reportSummary($);

MAIN:
{
    my $path_logs = shift;
    my $file_counts = shift;
    my %summary = ();
    
    parseLogPath($path_logs, \%summary);
    parseCounts($file_counts, \%summary);
    reportSummary(\%summary);
    
    exit 0;
}

sub parseLogPath($$)
{
    my $path_logs = $_[0];
    my $summary_ref = $_[1];
    
    opendir(my $dh, $path_logs) or die $!;
    while(my $file = readdir($dh))
    {
        # filter for text files
        next unless (-f "$path_logs/$file");
        next unless ($file =~ m/\.txt$/);
        
        # extract current file key
        my @label = split("\_", $file);
        my $key = $label[0];
        
        # parse each log file
        my ($countsRaw, $countsUnqAlgn) = parseLogFile("$path_logs/$file");
        $summary_ref->{$label[0]}{"raw"} = $countsRaw;
        $summary_ref->{$label[0]}{"unqalgn"} = $countsUnqAlgn;
        $summary_ref->{$label[0]}{"gene"} = 0;
        $summary_ref->{$label[0]}{"ercc"} = 0;
        
    }
    closedir($dh);
    
}

sub parseLogFile($)
{
    my $file_log = $_[0];
    my $countsRaw = 0;
    my $countsUnqAlgn = 0;
    open(my $fh, "<", $file_log) or die $!;
    while(<$fh>)
    {
        chomp($_);
        
        if($_ =~ m/Number of input reads \|\s+([0-9]+)/g)
        {
            $countsRaw = $1;
        }
        
        if($_ =~ m/Uniquely mapped reads number \|\s+([0-9]+)/g)
        {
            $countsUnqAlgn = $1;
        }
        
    }
    close($fh);
    
    return ($countsRaw, $countsUnqAlgn);
}

sub parseCounts($$)
{
    my $file_counts = $_[0];
    my $summary_ref = $_[1];
    
    my @keyPrimary = ();
    
    open(my $fh, "<", $file_counts) or die $!;
    while(<$fh>)
    {
        chomp($_);
        
        # skip commands
        next if($_ =~ m/^#/);
        
        # split header
        if($_ =~ m/^Geneid/)
        {
            my @header = split("\t", $_);
            @header = @header[6..$#header];
            
            # parse header
            foreach my $label (@header)
            {
                my @tmp = split("\/",$label);
                @tmp = split("\_", $tmp[-1]);
                my $key = $tmp[0];
                push(@keyPrimary, $key);
                
            }
            
        }
        else
        {
            my @line = split("\t", $_);
            @line = @line[6..$#line];
            
            my $keySecondary = ($_ =~ m/^ERCC\-/) ? "ercc" : "gene";
            
            for(my $i = 0; $i <= $#line; $i++)
            {
                $summary_ref->{$keyPrimary[$i]}{$keySecondary} += $line[$i];
            }
        }
        
    }
    close($fh);
    
}

sub reportSummary($)
{
    my $summary_ref = $_[0];
    
    # header
    print "sample\traw.reads\tunq.aligned.reads\tannotated.reads\tercc.reads\tunq.aligned.\%\tannotated.\%\tercc.\%\n";
    
    foreach my $keyid (sort keys %{$summary_ref})
    {
        # counts
        my $countsRaw = exists($summary_ref->{$keyid}{"raw"}) ? $summary_ref->{$keyid}{"raw"} : 0;
        my $countsUnqAlgn = exists($summary_ref->{$keyid}{"unqalgn"}) ? $summary_ref->{$keyid}{"unqalgn"} : 0;
        my $countsGene = exists($summary_ref->{$keyid}{"gene"}) ? $summary_ref->{$keyid}{"gene"} : 0;
        my $countsErcc = exists($summary_ref->{$keyid}{"ercc"}) ? $summary_ref->{$keyid}{"ercc"} : 0;
        
        # percentage
        my $percentUnqAlgn = (100 * $countsUnqAlgn)/$countsRaw;
        my $percentGene = (100 * $countsGene)/$countsRaw;
        my $percentErcc = (100 * $countsErcc)/$countsRaw;
        
        # output
        print $keyid,"\t",$countsRaw,"\t",$countsUnqAlgn,"\t",$countsGene,"\t",$countsErcc,"\t",sprintf("%.6f",$percentUnqAlgn),"\t",sprintf("%.6f",$percentGene),"\t",sprintf("%.6f",$percentErcc),"\n";
    }
    
}

